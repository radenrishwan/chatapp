module github.com/radenrishwan/chatapp

go 1.21.1

require (
	github.com/go-chi/chi/v5 v5.0.10 // indirect
	github.com/klauspost/compress v1.10.3 // indirect
	golang.org/x/tools v0.13.0
	nhooyr.io/websocket v1.8.7 // indirect
)
