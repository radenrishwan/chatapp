package chat

import (
	"context"
	"log"

	"github.com/radenrishwan/chatapp"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

type ChatService struct {
	Users      map[string]*User
	Subcribe   chan *User
	Unsubcribe chan string
	MSG        chan any
}

func NewChatService() *ChatService {
	return &ChatService{}
}

func (s *ChatService) Pool() {
	for {
		select {
		case msg := <-s.MSG:
			for _, user := range s.Users {
				ctx := context.Background()

				wsjson.Read(ctx, user.Conn, msg)
			}
		case user := <-s.Subcribe:
			// add user to room
			s.Users[user.ID] = user

			// send message to room
			for _, currentUser := range s.Users {
				if currentUser.ID == user.ID {
					continue
				}

				ctx := context.Background()

				err := wsjson.Read(ctx, currentUser.Conn, "an user has joined the room")
				if err != nil {
					chatapp.NewPanicError(ErrWriteMessage, err.Error(), "failed to send message to user pool")
				}
			}
		case userID := <-s.Unsubcribe:
			// remove user from room
			delete(s.Users, userID)

			// send message to room
			for _, currentUser := range s.Users {
				ctx := context.Background()

				err := wsjson.Read(ctx, currentUser.Conn, "an user has left the room")
				if err != nil {
					chatapp.NewPanicError(ErrWriteMessage, err.Error(), "failed to send message to user pool")
				}
			}
		}
	}
}

func (s *ChatService) UserPool(user *User) {
	defer func() {
		s.Unsubcribe <- user.ID
		user.Conn.Close(websocket.StatusNormalClosure, "connection closed")
		s.Unsubcribe <- user.ID
	}()

	// send welcome message
	ctx := context.Background()
	user.Conn.Write(ctx, websocket.MessageText, []byte("welcome to the room"))

	var msg any
	for {
		err := wsjson.Read(context.Background(), user.Conn, &msg)
		if err != nil {
			chatapp.NewPanicError(ErrReadMessage, err.Error(), "failed to read message from user pool")

			continue
		}

		log.Println("message received from ", user.ID, ": ", msg)
		s.MSG <- msg // send message to room
	}
}
