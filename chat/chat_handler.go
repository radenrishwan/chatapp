package chat

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/radenrishwan/chatapp"

	"nhooyr.io/websocket"
)

type ChatHandler struct {
	*ChatService
}

func NewChatHandler(chatService *ChatService) *ChatHandler {
	return &ChatHandler{
		ChatService: chatService,
	}
}

func (h *ChatHandler) UpgrateHandler(w http.ResponseWriter, r *http.Request) {
	c, err := websocket.Accept(w, r, nil)
	if err != nil {
		chatapp.NewPanicError(ErrWebsocketUpgrade, err.Error())
	}

	User := User{
		ID:   strconv.Itoa(time.Now().Nanosecond()),
		Name: "user",
		Conn: c,
	}

	h.Subcribe <- &User

	log.Println("user has joined the room with id : ", User.ID)

	go h.ChatService.UserPool(&User)
}
