package chat

import "errors"

var (
	ErrWebsocketUpgrade = errors.New("failed to upgrade the connection")
	ErrReadMessage      = errors.New("failed to read message")
	ErrWriteMessage     = errors.New("failed to write message")
)
