package chat

import "nhooyr.io/websocket"

type User struct {
	ID   string
	Name string
	Conn *websocket.Conn
}
