package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/radenrishwan/chatapp/chat"
)

func main() {
	chatService := chat.NewChatService()

	chatHandler := chat.NewChatHandler(chatService)

	router := chi.NewMux()

	// run websocket pool
	go chatService.Pool()

	router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
	})
	router.Get("/ws", chatHandler.UpgrateHandler)

	log.Println("server running on port 8080")
	http.ListenAndServe(":8080", router)
}
