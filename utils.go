package chatapp

import (
	"log"
)

func NewPanicError(err error, msg ...string) {
	for _, m := range msg {
		log.Println(m)
	}
	log.Panicln(err)
}
